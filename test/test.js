#!/usr/bin/env node

/* jshint esversion: 8 */
/* global describe */
/* global before */
/* global after */
/* global it */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

var ADMIN_USERNAME='admin';
var ADMIN_PASSWORD='changeme';
var BBB_LOCATION=path.resolve('bbb.mp4');
var LOCATION = 'test';

describe('Application life cycle test', function () {
    this.timeout(0);

    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000;

    var browser;
    var app;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    function waitForElement(elem) {
        return browser.wait(until.elementLocated(elem), TEST_TIMEOUT).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
        });
    }

    function setupWizard(callback) {
        browser.manage().deleteAllCookies().then(function () {
            return browser.get('https://' + app.fqdn);
        }).then(function () {
            return waitForElement(By.id('selectLocalizationLanguage'));
        }).then(function () {
            return browser.findElement(By.xpath('//button[@type="submit"]')).click();
        }).then(function () {
            return waitForElement(By.id('txtUsername'));
        }).then(function () {
            return browser.sleep(2000);
        }).then(function () {
            return browser.findElement(By.id('txtUsername')).clear();
        }).then(function () {
            return browser.findElement(By.id('txtUsername')).sendKeys(ADMIN_USERNAME);
        }).then(function () {
            return browser.findElement(By.xpath('//div[@id="wizardUserPage" ]//input[@id="txtManualPassword"]')).clear();
        }).then(function () {
            return browser.findElement(By.xpath('//div[@id="wizardUserPage" ]//input[@id="txtManualPassword"]')).sendKeys(ADMIN_PASSWORD);
        }).then(function () {
            return browser.findElement(By.id('txtPasswordConfirm')).clear();
        }).then(function () {
            return browser.findElement(By.id('txtPasswordConfirm')).sendKeys(ADMIN_PASSWORD);
        }).then(function () {
            return browser.findElement(By.xpath('//div[@id="wizardUserPage" ]//button[.//span[contains(text(), "Next")]]')).click();
        }).then(function () {
            return waitForElement(By.xpath('//div[contains(@class, "editLibrary")]'));
        }).then(function () {
            return browser.findElement(By.xpath('//div[contains(@class, "editLibrary")]')).click();
        }).then(function () {
            return waitForElement(By.id('selectCollectionType'));
        }).then(function () {
            return browser.executeScript('document.getElementById("selectCollectionType").selectedIndex = 1');
        }).then(function () {
            return browser.findElement(By.id('txtValue')).sendKeys('Movies');
        }).then(function () {
            return browser.findElement(By.xpath('//button[contains(@class, "btnAddFolder")]')).click();
        }).then(function () {
            return waitForElement(By.id('txtDirectoryPickerPath'));
        }).then(function () {
            return browser.findElement(By.id('txtDirectoryPickerPath')).sendKeys('/app/data/files/Movies');
        }).then(function () {
            return browser.findElement(By.xpath('//button[contains(@class, "btnRefreshDirectories")]')).click();
        }).then(function () {
            return browser.findElement(By.xpath('//form[.//input[@id="txtDirectoryPickerPath"]]')).submit();
        }).then(function () {
            return waitForElement(By.xpath('//button[.//span[contains(text(), "Ok")]]'));
        }).then(function () {
            return browser.findElement(By.xpath('//button[.//span[contains(text(), "Ok")]]')).click();
        }).then(function () {
            return waitForElement(By.xpath('//div[@id="wizardLibraryPage"]//button[.//span[contains(text(), "Next")]]'));
        }).then(function () {
            return browser.sleep(2000);
        }).then(function () {
            return browser.findElement(By.xpath('//div[@id="wizardLibraryPage"]//button[.//span[contains(text(), "Next")]]')).click();
        }).then(function () {
            return waitForElement(By.xpath('//div[.//h1[contains(text(), "Preferred Metadata Language")] and @id="wizardSettingsPage" ]//button[.//span[contains(text(), "Next")]]'));
        }).then(function () {
            return browser.sleep(2000);
        }).then(function () {
            return browser.findElement(By.xpath('//div[.//h1[contains(text(), "Preferred Metadata Language")] and @id="wizardSettingsPage" ]//button[.//span[contains(text(), "Next")]]')).click();
        }).then(function () {
            return waitForElement(By.xpath('//div[.//h1[contains(text(), "Configure Remote Access")] and @id="wizardSettingsPage" ]//button[.//span[contains(text(), "Next")]]'));
        }).then(function () {
            return browser.sleep(2000);
        }).then(function () {
            return browser.findElement(By.xpath('//div[.//h1[contains(text(), "Configure Remote Access")] and @id="wizardSettingsPage" ]//button[.//span[contains(text(), "Next")]]')).click();
        }).then(function () {
            return waitForElement(By.xpath('//button[.//span[contains(text(), "Finish")]]'));
        }).then(function () {
            return browser.findElement(By.xpath('//button[.//span[contains(text(), "Finish")]]')).click();
        }).then(function () {
            return waitForElement(By.id('txtManualName'));
        }).then(function () {
            callback();
        });
    }

    function login(username, password, callback) {
        browser.manage().deleteAllCookies().then(function () {
            return browser.get('https://' + app.fqdn);
        }).then(function () {
            return waitForElement(By.id('txtManualName'));
        }).then(function () {
            return browser.findElement(By.id('txtManualName')).sendKeys(username);
        }).then(function () {
            return browser.findElement(By.id('txtManualPassword')).sendKeys(password);
        }).then(function () {
            return browser.findElement(By.xpath('//button[@type="submit"]')).click();
        }).then(function () {
            return waitForElement(By.xpath('//div[text() = "Home"]'));
        }).then(function () {
            callback();
        });
    }

    function logout(callback) {
        browser.get('https://' + app.fqdn + '/web/index.html#!/mypreferencesmenu.html').then(function () {
            return waitForElement(By.className('mainDrawerButton'));
        }).then(function () {
            return browser.sleep(1000);
        }).then(function () {
            return browser.findElement(By.className('mainDrawerButton')).click();
        }).then(function () {
            return waitForElement(By.className('btnLogout'));
        }).then(function () {
            return browser.sleep(1000);
        }).then(function () {
            return browser.findElement(By.className('btnLogout')).click();
        }).then(function () {
            return browser.sleep(2000);
        }).then(function () {
            callback();
        });
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    function contentAdd(callback) {
	    execSync(`cloudron push ${BBB_LOCATION} /app/data/files/Movies/BBB.mp4 --app ${LOCATION}`, EXEC_ARGS);

        // jellyfin takes it's sweet little time to scan...
        console.log('Waiting for 60 seconds for scan');
        setTimeout(callback, 60000);
    }

    function contentExists(callback) {
        browser.get('https://' + app.fqdn + '/web/index.html').then(function () {
            return waitForElement(By.xpath("//button[@title='BBB']"));
        }).then(function () {
            callback();
        });
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('setup wizard', setupWizard);
    it('can login as admin', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can logout', logout);

    it('can login', login.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('can add content', contentAdd);
    it('content exists', contentExists);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can login', login.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('content exists', contentExists);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function (done) {
        execSync(`cloudron restore --app ${app.id}`, EXEC_ARGS);

        // needs some time to fully start up
        setTimeout(done, 5000);
    });

    it('can login', login.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('content exists', contentExists);
    it('can logout', logout);

    it('move to different location', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('content exists', contentExists);
    it('can logout', logout);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    // test update
    it('install app', function () { execSync(`cloudron install --appstore-id org.jellyfin.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('setup wizard', setupWizard);
    it('can login', login.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('can add content', contentAdd);
    it('content exists', contentExists);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('content exists', contentExists);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });
});
