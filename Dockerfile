FROM cloudron/base:3.0.0@sha256:455c70428723e3a823198c57472785437eb6eab082e79b3ff04ea584faf46e92

RUN mkdir -p /app/pkg
WORKDIR /app/pkg

# https://repo.jellyfin.org/releases/server/
ARG JELLYFIN_VERSION="10.7.7-1"
# https://repo.jellyfin.org/releases/plugin/ldap-authentication/
ARG JELLYFIN_LDAP_VERSION="10.0.0.0"

ENV DEBIAN_FRONTEND="noninteractive"
ENV NVIDIA_DRIVER_CAPABILITIES="compute,video,utility"

# https://github.com/jellyfin/jellyfin/issues/6686 (ca-certificates)
RUN curl -o /tmp/key.gpg.key https://repo.jellyfin.org/ubuntu/jellyfin_team.gpg.key && apt-key add /tmp/key.gpg.key && \
    apt-get update && \
    apt install -y ca-certificates && \
    echo 'deb [arch=amd64] https://repo.jellyfin.org/ubuntu focal main' > /etc/apt/sources.list.d/jellyfin.list && \
    apt-get update && \
    apt-get install -y --no-install-recommends at i965-va-driver jellyfin-ffmpeg mesa-va-drivers && \
    apt-get install -y "jellyfin=${JELLYFIN_VERSION}" && \
    mkdir -p /tmp/jellyfin_ldap /app/code/jellyfin_ldap && \
    wget "https://repo.jellyfin.org/releases/plugin/ldap-authentication/ldap-authentication_${JELLYFIN_LDAP_VERSION}.zip" -O /tmp/jellyfin_ldap/jellyfin_ldap.zip && \
    unzip /tmp/jellyfin_ldap/jellyfin_ldap.zip -d /app/code/jellyfin_ldap && \
    rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*

# Workaround for https://github.com/jellyfin/jellyfin/issues/3638 / https://github.com/jellyfin/jellyfin/issues/3956
RUN ln -s /usr/share/jellyfin/web/ /usr/lib/jellyfin/bin/jellyfin-web

# Jellyfin needs to write plugin manifest stuff
RUN mv /app/code/jellyfin_ldap/meta.json /app/code/jellyfin_ldap/meta.json.orig && \
    ln -s /app/data/jellyfin/jellyfin_ldap.meta.json /app/code/jellyfin_ldap/meta.json

COPY start.sh /app/pkg/

CMD ["/app/pkg/start.sh"]
